module blah

go 1.12

require (
	github.com/andersfylling/disgord v0.10.3
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lib/pq v1.1.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480 // indirect
	golang.org/x/net v0.0.0-20190415214537-1da14a5a36f2 // indirect
	golang.org/x/sys v0.0.0-20190418153312-f0ce4c0180be // indirect
	google.golang.org/appengine v1.5.0 // indirect
)
