FROM golang:latest AS build

COPY src/ /src/

WORKDIR /src

RUN go build --tags netgo --ldflags '-w -linkmode external -extldflags "-static"' -a